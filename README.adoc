= AmigaDOS Inside and Out

This is the digital version of the book `AmigaDOS Inside and Out`

*Author:* Ruediger Kerkloh, Manfred Tornsdorf, Bernd Zoller

*Year:* 1990

*Publisher:* Abacus, Data Becker

Released under *Creative Commons* 2019.

== Description
_AmigaDOS - You'll learn about the AmigaDOS Shell, the user interface of AmigaDOS._

== Read this book: 

* https://gitlab.com/amigasourcecodepreservation/amigados-inside-and-out/blob/master/pdf/amigados-inside-and-out-1990-kerkloh-tornsdorf-zoller.pdf[scanned pdf]
* https://gitlab.com/amigasourcecodepreservation/amigados-inside-and-out/blob/master/pdf/amigados-inside-and-out-revised-1991-kerkloh-tornsdorf-zoller.pdf[scanned pdf - revised]

== Contributing

If you'd like to help out, here is a suggested to-do:

* Convert the original scan to AsciiDoc.
 
== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

== Copyright & Author

Copyright 1990 Ruediger Kerkloh, Manfred Tornsdorf, Bernd Zoeller

= Credits

Ruediger Kerkloh, Manfred Tornsdorf, Bernd Zoeller

Dr. Achim Becker.

Arnie Lee.

Bombjack for the scans.

